﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json.Linq;
using System.Linq;

public class LoadJSON : MonoBehaviour
{
    [SerializeField] private JsonOutputWidget output = null;
    private string path;

    private void Start()
    {
        path = Path.Combine(Application.streamingAssetsPath, "JsonChallenge.json");
        LoadData();
    }

    public void LoadData()
    {
        string json = File.ReadAllText(path);
        JObject parsed = JObject.Parse(json);
        string title = (string)parsed["Title"];

        JArray headersParsed = (JArray)parsed["ColumnHeaders"];
        List<string> headers = headersParsed.Select(c => (string)c).ToList();

        var rows = new List<Dictionary<string, string>>();
        foreach (var rowParsed in parsed["Data"])
        {
            var row = new Dictionary<string, string>();
            foreach (string headerName in headers)
            {
                row[headerName] = rowParsed[headerName]?.ToString();
            }
            rows.Add(row);
        }

        output.Refresh(title, headers, rows);
    }

}
