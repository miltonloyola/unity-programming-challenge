﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class JsonOutputWidget : MonoBehaviour
{
    [SerializeField] private Text title = null;
    [SerializeField] private Transform headers = null;
    [SerializeField] private Transform table = null;
    [SerializeField] private Transform rowPrefab = null;
    [SerializeField] private Transform boldCellPrefab = null;
    [SerializeField] private Transform normalCellPrefab = null;

    public void Refresh(string titleText, List<string> headerNames, List<Dictionary<string, string>> rowsData)
    {
        // Title
        title.text = titleText;
        // Headers
        Clear(headers);
        foreach (string headerName in headerNames)
        {
            NewCell(boldCellPrefab, headers, headerName);
        }
        // Rows
        Clear(table);
        foreach (var rowData in rowsData)
        {
            var row = Instantiate(rowPrefab, table, false);
            Clear(row);
            foreach (string headerName in headerNames)
            {
                NewCell(normalCellPrefab, row, rowData[headerName]);
            }
        }
    }

    private void NewCell(Transform original, Transform parent, string content)
    {
        var clone = Instantiate(original, parent, false);
        clone.GetChild(0).GetComponent<TextMeshProUGUI>().text = content;
    }

    private void Clear(Transform parent)
    {
        foreach (Transform child in parent) GameObject.Destroy(child.gameObject);
    }

}
